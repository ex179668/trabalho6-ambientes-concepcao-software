package br.unicamp.ic.inf335.app;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import org.bson.Document;

public class MongoAppMain {
	
	private static final String USER 		= "mongoAdmin";
	private static final String PASS 		= "INF335UNICAMP";
	private static final String HOST 		= "localhost";
	private static final String DATABASE 	= "admin";

	public static void main(String[] args) {
		
		System.out.println("Conectando com o MongoDB");
		try (MongoClient client = conectar()) {
			
			System.out.println("Conectando a base " + DATABASE);
			MongoDatabase db = client.getDatabase(DATABASE);
			
			System.out.println("Obtendo a collection de produtos");
			MongoCollection<Document> produtosCollection = db.getCollection("produtos");
			
			System.out.println("Imprimindo Produtos");
			listarProdutos(produtosCollection);
			
			System.out.println("Inserindo Produto");
			inserirProduto(produtosCollection, "TempProd", "Bla bla", "1650.0", "Bla bla");
			
			System.out.println("Imprimindo Produtos novamente");
			listarProdutos(produtosCollection);
			
			System.out.println("Editando o Produto TempProd");
			editarPrecoProduto(produtosCollection, "TempProd", "3781.0");
			
			System.out.println("Imprimindo Produtos apos alteracao");
			listarProdutos(produtosCollection);
			
			System.out.println("Deletando o Produto TempProd");
			deletarProdutoPorNome(produtosCollection, "TempProd");
			
			System.out.println("Imprimindo Produtos apos delete");
			listarProdutos(produtosCollection);
		}
		
	}
	
	private static MongoClient conectar() {
		return MongoClients.create(String.format("mongodb://%s:%s@%s", USER, PASS, HOST));
	}
	
	private static void listarProdutos(MongoCollection<Document> produtosCollection) {
		FindIterable<Document> produtos = produtosCollection.find();
		for (Document p : produtos) {
			System.out.println(p.getString("nome") + " -- "
					+ p.getString("descricao")  + " -- " 
					+ p.getString("valor")  + " -- "
					+ p.getString("estado")  + " -- ");
		}
	}
	
	private static void inserirProduto(MongoCollection<Document> produtosCollection, String nome, String descricao, String valor, String estado) {
		Document produto = new Document();
		produto.append("nome", nome)
			.append("descricao", descricao)
			.append("valor", valor)
			.append("estado", estado);
		
		produtosCollection.insertOne(produto);
	}
	
	private static void editarPrecoProduto(MongoCollection<Document> produtosCollection, String nome,
			String novoValor) {
		produtosCollection.updateOne(Filters.eq("nome", nome), Updates.set("valor", novoValor));
	}
	
	private static void deletarProdutoPorNome(MongoCollection<Document> produtosCollection, String nome) {
		produtosCollection.deleteOne(Filters.eq("nome", nome));
	}
}
